# Lignes Polygonales 

Ce projet contient le code source (sous la forme d'un projet maven d'application java) de l'exercice 1 du 
[TP JAVA](http://lig-membres.imag.fr/genoud/teaching/PL2AI/tds/POO/sujets/tp13_Collections/tp13_Collections.html#section01) sur le collections du cours Java du M2CCI.

Pour récupérer les sources de ce projet vous pouvez

* soit utiliser git en effectuant la commande 
   ```
   git clone git@gricad-gitlab.univ-grenoble-alpes.fr:enseignement1/m2cci/java/lignespolygonales.git
   ```
* soit récupérer une archive (.tar.gz ou .zip) de l'une des releases du projet

## Dernière release

- [Release 1.1](https://gricad-gitlab.univ-grenoble-alpes.fr/enseignement1/m2cci/java/lignespolygonales/-/releases/v1.1) :
  - implémentation complète de la classe *LignePoygonale*.

