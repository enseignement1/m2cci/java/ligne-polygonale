package fr.im2ag.m2cci.geom;

import java.util.ArrayList;
import java.util.List;

/**
 * Cette classe représente un ligne polygonale (ligne brisée) avec les
 * contraintes suivantes:
 *
 * - la ligne contient au moins deux points,
 *
 * - un même sommet peut être partagé par plusieurs lignes polygonales,
 *
 * - si un sommet peut appartenir à plusieurs lignes polygonales, il n'est pas
 * contre pas possible qu'il apparaisse plusieurs fois dans la même ligne
 * polygonale.
 *
 */
public class LignePolygonale {

    private List<Point> sommets = new ArrayList<>();

    // --------------------------------------------------------------------------
    // Constructeurs (permettent de créer les objets)
    // --------------------------------------------------------------------------
    /**
     * initialise une ligne polygonale avec un segment de droite spécifié par
     * deux points.
     *
     * @param p1 point origine du segment
     * @param p2 point extrémité du segment
     *
     * @throws IllegalArgumentException si le segment initial est dégénéré
     *                                  (c'est à dire si le point origine est
     *                                  confondu avec le point extrémité)
     */
    public LignePolygonale(Point p1, Point p2) {
        if (p1.equals(p2)) {
            throw new IllegalArgumentException("polygone dégénéré");
        }
        sommets.add(p1);
        sommets.add(p2);
    }

    /**
     * initialise une ligne polygonale avec un segment de droite spécifié par
     * les coordonnées cartésiennes de ses deux extrémités.
     *
     * @param x1 abscisse du point origine du segment
     * @param y1 ordonnée du point origine du segment
     * @param x2 abscisse du point extrémité du segment
     * @param y2 ordonnée du point extrémité du segment
     *
     * @throws IllegalArgumentException si le segment initial est dégénéré
     *                                  (c'est à dire si le point origine est
     *                                  confondu avec le point extrémité)
     */
    public LignePolygonale(double x1, double y1, double x2, double y2) {
        this(new Point(x1, y1), new Point(x2, y2));
    }

    // --------------------------------------------------------------------------
    // Méthodes
    // --------------------------------------------------------------------------
    /**
     * retourne le sommet origine (le premier sommet) de la ligne polygonale
     *
     * @return le sommet origine
     */
    public Point getOrigine() {
        return sommets.get(0);
    }

    /**
     * retourne le sommet extrémité (le dernier sommet) de la ligne polygonale
     *
     * @return le sommet extrémité
     */
    public Point getExtremite() {
        return sommets.get(sommets.size() - 1);
    }

    /**
     * retourne le nombre de points de la ligne polygonale
     *
     * @return nombre de sommets de la ligne polygonale
     */
    public int getNbSommets() {
        return sommets.size();
    }

    /**
     * ajoute un sommet spécifié par ses coordonnées cartésiennes à la ligne
     * polygonale
     *
     * @param pt sommet ajouté à la ligne polygonale
     *
     * @return la ligne polygonale
     *
     * @throws IllegalArgumentException si le sommet ajouté est confondu avec
     *                                  l'un des sommets déjà existant dans la ligne
     *                                  polygonale
     */
    public LignePolygonale ajouterSommet(Point pt) {
        if (sommets.contains(pt)) {
            throw new IllegalArgumentException("sommet déjà existant");
        }
        sommets.add(pt);
        return this;
    }

    /**
     * ajoute un sommet spécifié par ses coordonnées cartésiennes à la ligne
     * polygonale
     *
     * @param x abscisse du sommet ajouté à la ligne polygonale
     * @param y ordonnée du sommet ajouté à la ligne polygonale
     *
     * @return la ligne polygonale
     *
     * @throws IllegalArgumentException si le sommet ajouté est confondu avec
     *                                  l'un des sommets déjà existant dans la ligne
     *                                  polygonale
     */
    public LignePolygonale ajouterSommet(double x, double y) {
        return ajouterSommet(new Point(x, y));
    }

    /**
     * renvoie le ième sommet de la ligne polygone (i dans l'intervale [1..n]
     *
     * @param i le numéro du sommet à récupérer
     * @return le ième sommet de la ligne polygonale
     *
     * @throws IndexOutOfBoundsException si i n'est pas dans l'intervalle [1..n]
     *                                   où n est le nombre de sommets de la ligne
     *                                   polygonale.
     */
    public Point getSommet(int i) {
        return sommets.get(i - 1);
    }

    /**
     * teste si la ligne polygonale contient un sommet identique à un point
     * donné.
     *
     * @param pt le point dont on cherche l'appartenance
     * @return true si la ligne a un sommet egale à pt, false sinon
     */
    public boolean contient(Point pt) {
        return sommets.contains(pt);
    }

    /**
     * permet de supprimer le ième sommet de la ligne, i appartenant à
     * l'intervalle [1..n] où n est le nombre de sommets de la ligne.
     *
     * @param i le numéro du sommet à supprimer
     *
     * @throws IllegalArgumentException  si la ligne ne contient que deux
     *                                   sommets.
     *
     * @throws IndexOutOfBoundsException si i n'estpas dans l'intervalle [1..n] où n
     *                                   est le nombre de sommets de la ligne
     *                                   polygonale.
     */
    public void supprimerSommet(int i) {
        if (this.sommets.size() < 3) {
            throw new IllegalArgumentException("La liste ne contient que 2 sommets");
        }
        sommets.remove(i - 1);
    }

    /**
     * retourne la longueur de la ligne polygonale (c'est à dire la somme des
     * longueurs de chacun des segments qui la composent).
     *
     * @return la longueur de la ligne polygonale
     */
    public double getLongueur() {
        double longueur = 0.0;
        for (int i = 0; i < sommets.size() - 1; i++) {
            longueur += sommets.get(i).distance(sommets.get(i + 1));
        }
        return longueur;
    }

    // --------------------------------------------------------------------------
    // redéfinition de méthodes héritées de Object
    // --------------------------------------------------------------------------

    /**
     * Renvoie un représentation textuelle chaîne de caractères) de la ligne
     * polygonale, plus précisément les coordonnées de chacun de ses sommets. 
     * Par exemple, pour une ligne de 4 sommets (0.0,0.0), (14.0,6.0), (25.0,-2.0)
     * et (5.0,-8.0), la chaîne retournée par cette méthode sera :
     * 
     * "LignePolygonale[1:(0.0,0.0),2:(14.0,6.0),3:(25.0,-2.0),4:(5.0,-8.0)]"
     *
     * @return une chaîne de caractères repésentant la ligne polygonale.
     */
    @Override
    public String toString() {
        // en utilisant des String que l'on concatène
        // String res = "LignePolygonale[";
        // for (int i = 0; i < sommets.size() - 1; i++) {
        // res += (i + 1) + ":" + sommets.get(i) + ",";
        // }
        // res += sommets.size() + ":" + sommets.get(sommets.size() - 1) + "]";
        // return res;
        // avec un StringBuilder, plus efficace quand beaucoup de concaténations
        StringBuilder res = new StringBuilder("LignePolygonale[");
        for (int i = 0; i < sommets.size() - 1; i++) {
            res.append(i + 1).append(":").append(sommets.get(i)).append(",");
        }
        res.append(sommets.size()).append(":").append(sommets.get(sommets.size() - 1)).append("]");
        return res.toString();
    }

    /**
     * teste l'égalité de la ligne avec une autre ligne. Deux lignes sont
     * considérées égales si elles ont le même nombre de sommets et que tous les
     * sommets de la première sont identiques aux sommets de la seconde et ce
     * dans le même ordre.
     *
     * @param obj l'objet à comparer avec cette ligne
     * @return true si obj est une ligne égale à this, false sinon
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LignePolygonale)) {
            return false;
        }
        LignePolygonale l = (LignePolygonale) obj;
        return this.sommets.equals(l.sommets);
    }

}
