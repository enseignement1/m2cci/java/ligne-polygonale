package fr.im2ag.m2cci.geom;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Philippe GENOUD - Université Grenoble Alpes (UGA) - Laboratoire
 * Informatique de Grenoble (LIG) - équipe STeamer
 */
public class LignePolygonaleTest {

    public LignePolygonaleTest() {
    }

    /**
     * Teste la construction d'une ligne avec le constructeur
     * LignePolygonale(double, double, double, double).
     *
     * Ce test vérifie que la construction avec deux points identiques provoque
     * bien une exception de typeIllegalArgumentException.
     */
    @Test
    public void test1() {
        assertThrows(IllegalArgumentException.class, () -> new LignePolygonale(10.0, 14.0, 10.0, 14.0));
    }

    /**
     * Teste la construction d'une ligne avec le constructeur
     * LignePolygonale(Point,Point). Ce test vérifie que la construction avec
     * deux points identiques provoque bien une exception de
     * typeIllegalArgumentException.
     */
    @Test
    public void test2() {
        assertThrows(IllegalArgumentException.class, () -> new LignePolygonale(new Point(10.0, 14.0), new Point(10.0, 14.0)));
    }

    /**
     * Teste la méthode getOrigine sur une ligne construite en utilisant le
     * constructeur LignePolygonale(double, double, double, double). Vérifie que
     * les deux premiers paramètres définissent bien le point origine
     */
    @Test
    public void test3() {
        LignePolygonale ligne = new LignePolygonale(10.0, 14.0, 20.5, -4.3);
        assertEquals(new Point(10.0, 14.0), ligne.getOrigine());
    }

    /**
     * Teste la méthode getOrigine sur une ligne construite en utilisant le
     * constructeur LignePolygonale(Point, Point). Vérifie que le premier
     * paramètres définit bien le point origine
     */
    @Test
    public void test4() {
        LignePolygonale ligne = new LignePolygonale(new Point(10.0, 14.0), new Point(20.5, -4.3));
        assertEquals(new Point(10.0, 14.0), ligne.getOrigine());
    }

    /**
     * Teste la méthode getExtremite sur une ligne construite en utilisant le
     * constructeur LignePolygonale(double, double, double, double)
     */
    @Test
    public void test5() {
        LignePolygonale ligne = new LignePolygonale(10.0, 14.0, 20.5, -4.3);
        assertEquals(new Point(20.5, -4.3), ligne.getExtremite());
    }

    /**
     * Teste la méthode getExtremite sur une ligne construite en utilisant le
     * constructeur LignePolygonale(Point, Point)
     */
    @Test
    public void test6() {
        LignePolygonale ligne = new LignePolygonale(new Point(10.0, 14.0), new Point(20.5, -4.3));
        assertEquals(new Point(20.5, -4.3), ligne.getExtremite());
    }

    /**
     * Teste la méthode getNbPoints sur une ligne construite en utilisant le
     * constructeur LignePolygonale(double, double, double, double)
     */
    @Test
    public void test7() {
        LignePolygonale ligne = new LignePolygonale(10.0, 14.0, 20.5, -4.3);
        assertEquals(2, ligne.getNbSommets());
    }

    /**
     * Teste la méthode getNbPoints sur une ligne construite en utilisant le
     * constructeur LignePolygonale(Point, Point)
     */
    @Test
    public void test8() {
        LignePolygonale ligne = new LignePolygonale(new Point(10.0, 14.0), new Point(20.5, -4.3));
        assertEquals(2, ligne.getNbSommets());
    }

    /**
     * Teste la méthode ajouterPoint(double, double)
     */
    @Test
    public void test9() {
        LignePolygonale ligne = new LignePolygonale(10.0, 14.0, 20.5, -4.3);
        ligne.ajouterSommet(34.0, 17.1);
        assertEquals(3, ligne.getNbSommets());
        assertEquals(new Point(10.0, 14.0), ligne.getOrigine());
        assertEquals(new Point(34.0, 17.1), ligne.getExtremite());
        ligne.ajouterSommet(32.0, 55.1);
        assertEquals(4, ligne.getNbSommets());
        assertEquals(new Point(10.0, 14.0), ligne.getOrigine());
        assertEquals(new Point(32.0, 55.1), ligne.getExtremite());

        ligne.ajouterSommet(37.5, 56.7).ajouterSommet(45.6, -45.1);
        assertEquals(6, ligne.getNbSommets());
        assertEquals(new Point(10.0, 14.0), ligne.getOrigine());
        assertEquals(new Point(45.6, -45.1), ligne.getExtremite());
    }

    /**
     * Teste la méthode ajouterPoint(Point)
     */
    @Test
    public void test10() {
        LignePolygonale ligne = new LignePolygonale(10.0, 14.0, 20.5, -4.3);
        ligne.ajouterSommet(new Point(34.0, 17.1));
        assertEquals(3, ligne.getNbSommets());
        assertEquals(new Point(10.0, 14.0), ligne.getOrigine());
        assertEquals(new Point(34.0, 17.1), ligne.getExtremite());
        ligne.ajouterSommet(new Point(32.0, 55.1));
        assertEquals(4, ligne.getNbSommets());
        assertEquals(new Point(10.0, 14.0), ligne.getOrigine());
        assertEquals(new Point(32.0, 55.1), ligne.getExtremite());

        ligne.ajouterSommet(new Point(37.5, 56.7)).ajouterSommet(new Point(45.6, -45.1));
        assertEquals(6, ligne.getNbSommets());
        assertEquals(new Point(10.0, 14.0), ligne.getOrigine());
        assertEquals(new Point(45.6, -45.1), ligne.getExtremite());
    }

    /**
     * Teste que la méthode ajouterPoint(double, double) provoque bien une
     * erreur si le point à ajouter est identique au point extremité de la
     * ligne.
     */
    @Test
    public void test11() {
        LignePolygonale ligne = new LignePolygonale(10.0, 14.0, 20.5, -4.3);
        assertThrows(IllegalArgumentException.class, () -> ligne.ajouterSommet(20.5, -4.3));
    }

    /**
     * Teste que la méthode ajouterPoint(Point) provoque bien une erreur si le
     * point à ajouter est identique au point extremité de la ligne.
     */
    @Test
    public void test12() {
        LignePolygonale ligne = new LignePolygonale(new Point(10.0, 14.0), new Point(20.5, -4.3));
        assertThrows(IllegalArgumentException.class, () -> ligne.ajouterSommet(new Point(20.5, -4.3)));
    }

    /**
     * test de la methode getSommet
     */
    @Test
    public void test13() {
        LignePolygonale ligne = new LignePolygonale(10.0, 14.0, 20.5, -4.3);
        ligne.ajouterSommet(new Point(12, -4))
                .ajouterSommet(new Point(-23.5, 28.1))
                .ajouterSommet(new Point(70.5, 3.05));
        assertEquals(new Point(10.0, 14.0), ligne.getSommet(1));
        assertEquals(new Point(20.5, -4.3), ligne.getSommet(2));
        assertEquals(new Point(12, -4), ligne.getSommet(3));
        assertEquals(new Point(-23.5, 28.1), ligne.getSommet(4));
        assertEquals(new Point(70.5, 3.05), ligne.getSommet(5));

        assertThrows(IndexOutOfBoundsException.class, () -> ligne.getSommet(6));
    }

    /**
     * teste la longueur d'un ligne polygonale
     */
    @Test
    public void test14() {
        LignePolygonale ligne = new LignePolygonale(0.0, 0.0, 10.0, 0.0);
        assertEquals(10.0, ligne.getLongueur(), 0.0);
        ligne.ajouterSommet(10.0, 24.5);
        assertEquals(34.5, ligne.getLongueur(), 0.0);
    }

    /**
     * test de la méthode contient
     */
    @Test
    public void test15() {
        LignePolygonale ligne = new LignePolygonale(10.0, 14.0, 20.5, -4.3);
        ligne.ajouterSommet(new Point(12, -4))
                .ajouterSommet(new Point(-23.5, 28.1))
                .ajouterSommet(new Point(70.5, 3.05));
        assertTrue(ligne.contient(new Point(10.0, 14.0)));
        assertTrue(ligne.contient(new Point(20.5, -4.3)));
        assertTrue(ligne.contient(new Point(12, -4)));
        assertTrue(ligne.contient(new Point(-23.5, 28.1)));
        assertTrue(ligne.contient(new Point(70.5, 3.05)));
        assertFalse(ligne.contient(new Point(44.5, 3.05)));
    }

    /**
     * Teste la méthode to String sur une ligne construite en utilisant le
     * constructeur LignePolygonale(double, double, double, double)
     */
    @Test
    public void test16() {
        System.out.println("toString");
        LignePolygonale ligne = new LignePolygonale(10.0, 14.0, 20.5, -4.3);
        String expResult = "LignePolygonale[1:(10.0,14.0),2:(20.5,-4.3)]";
        assertEquals(expResult, ligne.toString());
    }

    /**
     * Teste la méthode to String sur une ligne construite en utilisant le
     * constructeur LignePolygonale(Point, Point)
     */
    @Test
    public void test17() {
        System.out.println("toString");
        LignePolygonale ligne = new LignePolygonale(new Point(10.0, 14.0), new Point(20.5, -4.3));
        String expResult = "LignePolygonale[1:(10.0,14.0),2:(20.5,-4.3)]";
        assertEquals(expResult, ligne.toString());
    }

    /**
     * test de la méthode equals
     */
    @Test
    public void test18() {
        LignePolygonale ligne1 = new LignePolygonale(new Point(10.0, 14.0), new Point(20.5, -4.3));
        LignePolygonale ligne2 = new LignePolygonale(10.0, 14.0, 20.5, -4.3);
        // test que equals est reflexive
        assertTrue(ligne1.equals(ligne1));
        // teste que equals est symétrique quand deux lignes sont égales
        assertTrue(ligne1.equals(ligne2));
        assertTrue(ligne2.equals(ligne1));

        // teste que après modification de ligne 2 les deux lignes ne sont plus égales
        // teste que equals est symétrique quand deux lignes ne sont pas égales
        ligne2.ajouterSommet(new Point(14.0, 23.0));
        assertNotEquals(ligne1, ligne2);
        assertNotEquals(ligne2, ligne1);

        // teste qu'après modification de ligne 1 les deux lignes sont à nouveau égales
        ligne1.ajouterSommet(new Point(14.0, 23.0));
        assertEquals(ligne2, ligne1);
        assertEquals(ligne2, ligne1);

        // test de equals en passant par des références surclassées
        Object o1 = ligne1;
        Object o2 = ligne2;

        assertTrue(o1.equals(ligne2));
        assertTrue(ligne2.equals(o1));
        assertTrue(o1.equals(o2));

        ligne2.ajouterSommet(new Point(67.0, 23.0));
        assertFalse(o1.equals(ligne2));
        assertFalse(ligne2.equals(o1));
        assertFalse(o1.equals(o2));

        // test de equals avec des objets de type différent
        assertFalse(ligne1.equals(new Point(10.0, 14.0)));
    }

}
